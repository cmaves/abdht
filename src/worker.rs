use tokio::time::sleep_until;

use super::*;
use std::cell::RefCell;
use std::collections::{HashMap, HashSet, VecDeque};
use std::net::SocketAddr;
use std::str::FromStr;
use std::time::{Instant, SystemTime};

use futures::future::OptionFuture;
use futures::prelude::*;

pub struct ClientData<D: DHTIDHasher + Clone> {
    socket: UdpSocket,
    buckets: PeerBuckets<D>,
    hasher: D,
    addr: SocketAddr,
    outstanding_whoami: HashMap<SocketAddr, (usize, WhoAmIState)>,
    outstanding_findnode: HashMap<(SocketAddr, [u8; 32]), (usize, FindNodeState)>,
    cleanup: VecDeque<ToCleanup>,
    req_idx: usize,
}

impl<D: DHTIDHasher + Clone> ClientData<D> {
    pub fn new(socket: UdpSocket, hasher: D) -> Self {
        let addr = SocketAddr::from_str("0.0.0.0:0").unwrap();
        let now = SystemTime::now();
        ClientData {
            socket,
            buckets: PeerBuckets::new(hasher.clone(), addr, now),
            hasher,
            addr,
            outstanding_whoami: HashMap::new(),
            outstanding_findnode: HashMap::new(),
            cleanup: VecDeque::new(),
            req_idx: 0,
        }
    }

    pub fn set_addr(&mut self, addr: SocketAddr) {
        let now = SystemTime::now();
        self.addr = addr;
        self.buckets = PeerBuckets::new(self.hasher.clone(), self.addr, now);
    }

    pub fn id(&self, now: SystemTime) -> [u8; 32] {
        self.hasher.compute_id(self.addr, now)
    }

    pub async fn do_bootstrap(&mut self, seeds: &[SocketAddr]) -> Result<(), DHTError> {
        let peer_addrs = self.get_initial_address(seeds).await?;
        let now = SystemTime::now();
        let id = self.id(now);
        let mut msg_buf = [0; 1024];
        let findnode_msg = DhtReqs::FindNode(id, [0xFF; 32], 8)
            .serialize(&mut msg_buf)
            .unwrap();

        let search = Rc::new(RefCell::new(FindSelfSearch::new(now, None)));
        for boot_addr in peer_addrs {
            self.socket.send_to(findnode_msg, boot_addr).await?;
            let key = self.get_next_req_key();
            let state = FindNodeState::FindSelf(search.clone());
            self.outstanding_findnode
                .insert((boot_addr, self.id(now)), (key, state));
            self.cleanup
                .push_back(ToCleanup::new_find_node(boot_addr, id, key));
        }

        let to = sleep(Duration::from_secs(15));
        tokio::pin!(to);
        let mut msg_buf = [0; 1024];
        while !self.outstanding_whoami.is_empty() || !self.outstanding_findnode.is_empty() {
            tokio::select! {
                biased;
                res = self.socket.recv_from(&mut msg_buf) => {
                    let (len, peer_addr) = res?;
                    self.handle_request_msg(peer_addr, &msg_buf[..len]).await?;
                }
                _ = &mut to => {
                    break
                }
            }
        }
        if self.buckets.is_empty() {
            Err(DHTError::BootstrapFailure)
        } else {
            Ok(())
        }
    }

    async fn get_initial_address(
        &mut self,
        seeds: &[SocketAddr],
    ) -> Result<HashSet<SocketAddr>, DHTError> {
        let mut seeds: HashSet<SocketAddr> = HashSet::from_iter(seeds.iter().copied());

        let mut msg_buf = [0; 1024];
        let whoami_msg = DhtReqs::WhoAmI.serialize(&mut msg_buf).unwrap();
        for seed in seeds.iter() {
            self.socket.send_to(whoami_msg, *seed).await?;
        }

        let mut map: HashMap<SocketAddr, HashSet<SocketAddr>> = HashMap::new();
        let to = sleep(Duration::from_secs(5));
        tokio::pin!(to);
        let mut msg_buf = [0; 1024];
        while !seeds.is_empty() {
            tokio::select! {
                biased;
                res = self.socket.recv_from(&mut msg_buf) => {
                    let (len, peer_addr) = res?;
                    match DhtReqs::deserialize(&msg_buf[..len]) {
                        Ok(DhtReqs::WhoAmiRes(id, our_addr)) => {
                            let now = SystemTime::now();
                            let our_comp = self.hasher.compute_id(our_addr, now);
                            if our_comp != id || our_addr.is_ipv4() != peer_addr.is_ipv4() || !seeds.remove(&peer_addr) {
                                continue;
                            }
                            match map.get_mut(&our_addr) {
                                Some(v) => { v.insert(peer_addr); },
                                None => {
                                    let mut set = HashSet::new();
                                    set.insert(peer_addr);
                                    map.insert(our_addr, set);
                                }
                            }
                        }
                        Ok(DhtReqs::WhoAmI) => { self.handle_who_am_i(peer_addr).await.err(); },
                        _ => continue,
                    }
                }
                _ = &mut to => {
                    break
                }
            }
        }
        let (our_addr, peer_addrs) = map
            .into_iter()
            .max_by_key(|(_, s)| s.len())
            .ok_or(DHTError::BootstrapFailure)?;
        let now = SystemTime::now();
        self.buckets = PeerBuckets::new(self.hasher.clone(), self.addr, now);
        self.addr = our_addr;
        Ok(peer_addrs)
    }

    fn do_cleanup(&mut self) {
        let now = Instant::now();
        while let Some(tc) = self.cleanup.front() {
            if now.checked_duration_since(tc.deadline).is_none() {
                break;
            }
            match tc.typ {
                ReqType::WhoAmI(peer_addr) => {
                    if let Some((key, _)) = self.outstanding_whoami.get(&peer_addr) {
                        if *key == tc.key {
                            self.outstanding_whoami.remove(&peer_addr);
                        }
                    }
                }
                ReqType::FindNode(peer_addr, target) => {
                    if let Some((key, _)) = self.outstanding_findnode.get(&(peer_addr, target)) {
                        if *key == tc.key {
                            self.outstanding_findnode.remove(&(peer_addr, target));
                        }
                    }
                }
            }
            self.cleanup.pop_front();
        }
    }
    pub(super) async fn start(
        mut self,
        mut receiver: Receiver<ClientCmd<D>>,
    ) -> Result<Self, DHTError> {
        let mut msg_buf = [0; 1024];
        let mut shutdown = None;
        //let mut next_cleanup = None;
        loop {
            tokio::select! {
                biased;

                // handle local messages
                opt = receiver.recv() => match opt {
                    Some(ClientCmd::Ping(chan)) => { chan.send(()).ok(); }, // ignore err
                    Some(ClientCmd::Shutdown(chan)) => {
                        shutdown = Some(chan);
                        receiver.close();
                    },
                    Some(ClientCmd::PeerListLocal(chan)) => {
                        let mut ret = Vec::with_capacity(self.buckets.len());
                        ret.extend(self.buckets.iter().cloned());
                        let now = SystemTime::now();
                        ret.sort_unstable_by_key(|p| p.id(now));
                        chan.send(ret).ok(); // ignore err
                    }
                    Some(ClientCmd::Kill) => break,
                    None => break,
                },

                // handle incoming DHT requests from UdpSocket
                res = self.socket.recv_from(&mut msg_buf) => match res {
                    Ok((len, peer_addr)) => {
                        self.handle_request_msg(peer_addr, &msg_buf[..len]).await?;
                    }
                    Err(_) => break,
                },
            }
        }
        if let Some(chan) = shutdown {
            chan.send(()).err(); // ignore err
        }
        Ok(self)
    }
    async fn handle_request_msg(
        &mut self,
        peer_addr: SocketAddr,
        req_msg: &[u8],
    ) -> Result<(), DHTError> {
        match DhtReqs::deserialize(req_msg) {
            Ok(DhtReqs::WhoAmI) => self.handle_who_am_i(peer_addr).await,
            Ok(DhtReqs::WhoAmiRes(id, our_addr)) => {
                // TODO: use time gotten from the peer as the result
                let now = SystemTime::now();
                self.handle_who_am_i_res(id, our_addr, now, peer_addr).await
            }
            Ok(DhtReqs::FindNode(target, _, cnt)) => {
                self.handle_find_node(peer_addr, target, cnt).await
            }
            Ok(DhtReqs::FindNodeRes(target, node_addrs)) => {
                self.handle_find_node_res(peer_addr, target, node_addrs)
                    .await
            }
            Err(_) => Ok(()),
        }
    }
    async fn handle_who_am_i(&mut self, peer_addr: SocketAddr) -> Result<(), DHTError> {
        let now = SystemTime::now();
        let id = self.hasher.compute_id(peer_addr, now);
        let mut msg_buf = [0; 1024];
        let msg = DhtReqs::WhoAmiRes(id, peer_addr)
            .serialize(&mut msg_buf)
            .unwrap();
        self.socket.send_to(msg, peer_addr).await?;
        Ok(())
    }
    async fn handle_who_am_i_res(
        &mut self,
        id: [u8; 32],
        our_addr: SocketAddr,
        peer_time: SystemTime,
        peer_addr: SocketAddr,
    ) -> Result<(), DHTError> {
        let (_, state) = match self.outstanding_whoami.remove(&peer_addr) {
            Some(s) => s,
            None => return Ok(()),
        };
        if our_addr != self.addr {
            // TODO: handle this better
            return Ok(());
        }
        let now = SystemTime::now();
        let time_diff = now
            .duration_since(peer_time)
            .unwrap_or_else(|e| e.duration());
        if time_diff > Duration::from_secs(60 * 15) {
            return Ok(());
        }

        if id != self.id(peer_time) {
            return Ok(());
        }

        let id = self.id(now);
        let mut msg_buf = [0; 1024];
        match state {
            WhoAmIState::FindSelf(s) => {
                if !self.outstanding_findnode.contains_key(&(peer_addr, id)) {
                    let msg = DhtReqs::FindNode(id, [0xFF; 32], 8)
                        .serialize(&mut msg_buf)
                        .unwrap();
                    self.socket.send_to(msg, peer_addr).await?;
                    let key = self.get_next_req_key();
                    self.outstanding_findnode
                        .insert((peer_addr, id), (key, FindNodeState::FindSelf(s)));
                    self.cleanup
                        .push_back(ToCleanup::new_find_node(peer_addr, id, key))
                }
            }
            WhoAmIState::AddPeer => {
                if !self.outstanding_findnode.contains_key(&(peer_addr, id)) {
                    let msg = DhtReqs::FindNode(id, [0xFF; 32], 8)
                        .serialize(&mut msg_buf)
                        .unwrap();
                    self.socket.send_to(msg, peer_addr).await?;
                    let key = self.get_next_req_key();
                    self.outstanding_findnode
                        .insert((peer_addr, id), (key, FindNodeState::AddPeer));
                    self.cleanup
                        .push_back(ToCleanup::new_find_node(peer_addr, id, key))
                }
            }
            _ => todo!(),
        }
        Ok(())
    }

    async fn handle_find_node(
        &mut self,
        peer_addr: SocketAddr,
        target: [u8; 32],
        cnt: u8,
    ) -> Result<(), DHTError> {
        let now = SystemTime::now();
        let mut addrs = self.buckets.find_nearest_peers(target, cnt as usize, now);

        // we need to check if our own node should be included.
        let self_id = self.id(now);
        let i_idx = addrs
            .binary_search_by_key(&self_id, |p| p.id(now))
            .unwrap_err();
        let self_peer = Peer::new(self.hasher.clone(), self.addr);
        if i_idx == addrs.len() {
            if addrs.len() < cnt as usize {
                addrs.push(self_peer);
            }
        } else {
            addrs.pop();
            addrs.insert(i_idx, self_peer);
        }

        let addrs = addrs.into_iter().map(|a| a.addr());
        let mut msg_buf = [0; 1024];
        let msg = DhtReqs::FindNodeRes(target, addrs.collect())
            .serialize(&mut msg_buf)
            .unwrap();
        self.socket.send_to(msg, peer_addr).await?;
        self.add_node(now, peer_addr).await
    }
    async fn handle_find_node_res(
        &mut self,
        peer_addr: SocketAddr,
        target: [u8; 32],
        node_addrs: Vec<SocketAddr>,
    ) -> Result<(), DHTError> {
        let key = (peer_addr, target);
        let (_, state) = match self.outstanding_findnode.remove(&key) {
            Some(s) => s,
            None => return Ok(()),
        };
        let now = SystemTime::now();
        match state {
            FindNodeState::FindSelf(s) => {
                let peer = Peer::new(self.hasher.clone(), peer_addr);
                self.buckets.insert_into(now, peer);

                let mut msg_buf = [0; 1024];
                let msg = DhtReqs::WhoAmI.serialize(&mut msg_buf).unwrap();
                for node_addr in node_addrs {
                    if node_addr == self.addr {
                        continue;
                    }
                    let mut search = s.borrow_mut();
                    if search.searched.insert(node_addr) {
                        self.socket.send_to(msg, node_addr).await?;
                        let key = self.get_next_req_key();
                        self.outstanding_whoami
                            .insert(node_addr, (key, WhoAmIState::FindSelf(s.clone())));
                        self.cleanup
                            .push_back(ToCleanup::new_who_am_i(peer_addr, key));
                    }
                }
            }
            FindNodeState::AddPeer => {
                let peer = Peer::new(self.hasher.clone(), peer_addr);
                self.buckets.insert_into(now, peer);
                for node_addr in node_addrs {
                    self.add_node(now, node_addr).await?;
                }
            }
            _ => todo!(),
        }
        Ok(())
    }
    async fn add_node(&mut self, now: SystemTime, peer_addr: SocketAddr) -> Result<(), DHTError> {
        if peer_addr == self.addr {
            return Ok(());
        }
        let (_, can_insert) = self.buckets.known_peer(now, peer_addr);
        if !can_insert || self.outstanding_whoami.contains_key(&peer_addr) {
            return Ok(());
        }

        let mut msg_buf = [0; 1024];
        let msg = DhtReqs::WhoAmI.serialize(&mut msg_buf).unwrap();
        self.socket.send_to(msg, peer_addr).await?;
        let key = self.get_next_req_key();
        self.outstanding_whoami
            .insert(peer_addr, (key, WhoAmIState::AddPeer));
        self.cleanup
            .push_back(ToCleanup::new_who_am_i(peer_addr, key));
        Ok(())
    }
    fn get_next_req_key(&mut self) -> usize {
        let key = self.req_idx;
        self.req_idx = self.req_idx.wrapping_add(1);
        key
    }
}

enum WhoAmIState {
    FindSelf(Rc<RefCell<FindSelfSearch>>),
    AddPeer,
}
struct FindSelfSearch {
    now: SystemTime,
    searched: HashSet<SocketAddr>,
    _reset: CallOnDrop,
}

struct ToCleanup {
    deadline: Instant,
    key: usize,
    typ: ReqType,
}

enum ReqType {
    WhoAmI(SocketAddr),
    FindNode(SocketAddr, [u8; 32]),
}
impl ToCleanup {
    const REQ_TO: Duration = Duration::from_secs(5);
    fn new_who_am_i(addr: SocketAddr, key: usize) -> Self {
        ToCleanup {
            deadline: Instant::now() + Self::REQ_TO,
            key,
            typ: ReqType::WhoAmI(addr),
        }
    }
    fn new_find_node(addr: SocketAddr, id: [u8; 32], key: usize) -> Self {
        ToCleanup {
            deadline: Instant::now() + Self::REQ_TO,
            key,
            typ: ReqType::FindNode(addr, id),
        }
    }
}
impl FindSelfSearch {
    fn new(now: SystemTime, reset_fn: Option<Box<dyn FnOnce()>>) -> Self {
        Self {
            now,
            _reset: CallOnDrop(reset_fn),
            searched: HashSet::new(),
        }
    }
}
struct CallOnDrop(Option<Box<dyn FnOnce()>>);
impl Drop for CallOnDrop {
    fn drop(&mut self) {
        if let Some(drop_fn) = self.0.take() {
            drop_fn();
        }
    }
}
enum FindNodeState {
    FindSelf(Rc<RefCell<FindSelfSearch>>),
    FindKey([u8; 32]),
    AddPeer,
}

#[cfg(test)]
mod tests {
    use super::*;
    use tokio::runtime::Builder as RTBuilder;

    async fn setup_nb_client() -> (Sender<ClientCmd<hash::Sha256DHTHasher>>, SocketAddr) {
        let socket = UdpSocket::bind("127.0.0.1:0").await.unwrap();
        let nb_addr = socket.local_addr().unwrap();

        // create and start client
        let (nb_sender, nb_receiver) = channel(1);
        let hasher = hash::Sha256DHTHasher::new(0);
        std::thread::spawn(move || {
            let mut nb_client = ClientData::new(socket, hasher);
            nb_client.set_addr(nb_addr);
            let rt = RTBuilder::new_current_thread()
                .enable_all()
                .build()
                .unwrap();
            rt.block_on(nb_client.start(nb_receiver)).unwrap();
        });

        // ensure client is running
        let (one_sender, one_recv) = one_channel();
        nb_sender.send(ClientCmd::Ping(one_sender)).await.unwrap();
        one_recv.await.unwrap();
        (nb_sender, nb_addr)
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn dht_client_get_initial_address_single() {
        let (nb_sender, nb_addr) = setup_nb_client().await;
        println!("{}", nb_addr);

        let socket = UdpSocket::bind("127.0.0.1:0").await.unwrap();
        let init_addr = socket.local_addr().unwrap();
        let now = SystemTime::now();
        let hasher = hash::Sha256DHTHasher::new(0);
        let init_id = hasher.compute_id(init_addr, now);

        let mut init_client = ClientData::new(socket, hasher);
        init_client.get_initial_address(&[nb_addr]).await.unwrap();
        assert_eq!(init_addr, init_client.addr);
        assert_eq!(init_id, init_client.id(now));

        // clean-up nb_client
        nb_sender.send(ClientCmd::Kill).await.unwrap();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn dht_client_do_bootstrap_single() {
        let (nb_sender, nb_addr) = setup_nb_client().await;

        let socket = UdpSocket::bind("127.0.0.1:0").await.unwrap();
        let init_addr = socket.local_addr().unwrap();
        let now = SystemTime::now();
        let hasher = hash::Sha256DHTHasher::new(0);
        let init_id = hasher.compute_id(init_addr, now);

        let mut init_client = ClientData::new(socket, hasher);
        init_client.do_bootstrap(&[nb_addr]).await.unwrap();
        assert_eq!(init_addr, init_client.addr);
        assert_eq!(init_id, init_client.id(now));
        assert_eq!(1, init_client.buckets.len());

        // clean-up nb_client
        nb_sender.send(ClientCmd::Kill).await.unwrap();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn dht_client_do_bootstrap_multi() {
        todo!();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn dht_client_get_initial_address_mutli() {
        todo!();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn dht_client_find_peers() {
        todo!();
    }

    #[tokio::test(flavor = "multi_thread")]
    async fn dht_client_check_cleanup() {
        let (nb_sender, nb_addr) = setup_nb_client().await;

        let socket = UdpSocket::bind("127.0.0.1:0").await.unwrap();
        let init_addr = socket.local_addr().unwrap();

        let hasher = hash::Sha256DHTHasher::new(0);
        let now = SystemTime::now();
        let mut init_client = ClientData::new(socket, hasher.clone());
        init_client.addr = init_addr;
        init_client.buckets = PeerBuckets::new(hasher.clone(), init_addr, now);

        let nb_peer = Peer::new(hasher.clone(), nb_addr);
        let addr0 = SocketAddr::from_str("127.0.0.1:80").unwrap();
        let addr1 = SocketAddr::from_str("127.0.0.1:443").unwrap();
        let peer0 = Peer::new(hasher.clone(), addr0);
        let peer1 = Peer::new(hasher, addr1);

        init_client.buckets.insert_into(now, nb_peer.clone());
        init_client.buckets.insert_into(now, peer0);
        init_client.buckets.insert_into(now, peer1);

        let (sender, receiver) = channel(1);
        let task = tokio::task::spawn(async move {
            todo!("Trigger cleanup");
            let target = nb_peer.id(now);
            tokio::time::sleep(Duration::from_secs(6)).await;
            sender.send(ClientCmd::Kill).await
        });
        init_client = init_client.start(receiver).await.unwrap();
        task.await.unwrap().unwrap();
        assert_eq!(0, init_client.outstanding_whoami.len());
        assert_eq!(0, init_client.outstanding_findnode.len());

        nb_sender.send(ClientCmd::Kill).await.unwrap();
    }
}
