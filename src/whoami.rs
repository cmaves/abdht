use std::collections::{BTreeSet, HashMap};
use std::net::SocketAddr;
use std::time::Instant;

#[derive(Clone, Copy)]
struct WhoAmIInfo {
    updated: Instant,
    cnt: u64,
    addr: SocketAddr,
}

impl Ord for WhoAmIInfo {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let now = Instant::now();
        let left = self
            .cnt
            .saturating_sub(now.duration_since(self.updated).as_secs());
        let right = other
            .cnt
            .saturating_sub(now.duration_since(other.updated).as_secs());
        left.cmp(&right).then(self.addr.cmp(&other.addr))
    }
}

impl PartialOrd for WhoAmIInfo {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for WhoAmIInfo {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other).is_eq()
    }
}
impl Eq for WhoAmIInfo {}

struct WhoAmIList {
    map: HashMap<SocketAddr, WhoAmIInfo>,
    set: BTreeSet<WhoAmIInfo>,
    max_size: usize,
}

impl WhoAmIList {
    fn new(max_size: usize) -> Self {
        WhoAmIList {
            map: HashMap::new(),
            set: BTreeSet::new(),
            max_size,
        }
    }
    fn update(&mut self, addr: SocketAddr) -> SocketAddr {
        let updated = Instant::now();
        let map_info = match self.map.get_mut(&addr) {
            Some(info) => {
                let b = self.set.remove(&info);
                debug_assert!(b);
                info.cnt = info
                    .cnt
                    .saturating_sub(updated.duration_since(info.updated).as_secs());
                info.updated = updated;
                info.cnt += 1;
                *info
            }
            None => WhoAmIInfo {
                addr,
                cnt: 1,
                updated,
            },
        };
        let not_present = self.set.insert(map_info);
        assert!(not_present);
        self.first_addr().unwrap()
    }
    fn len(&self) -> usize {
        debug_assert_eq!(self.map.len(), self.set.len());
        self.map.len()
    }
    fn first_addr(&self) -> Option<SocketAddr> {
        self.set.iter().next_back().map(|info| info.addr)
    }
}
