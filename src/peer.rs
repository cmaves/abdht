use std::net::SocketAddr;
use std::time::SystemTime;

use super::*;

#[derive(Clone, Debug)]
pub struct Peer<D>
where
    D: DHTIDHasher + Clone,
{
    addr: SocketAddr,
    hasher: D,
}

impl<D: DHTIDHasher + Clone> Peer<D> {
    pub fn new(hasher: D, addr: SocketAddr) -> Self {
        Peer { addr, hasher }
    }
    pub fn id(&self, now: SystemTime) -> [u8; 32] {
        self.hasher.compute_id(self.addr, now)
    }
    pub fn addr(&self) -> SocketAddr {
        self.addr
    }
}

pub const KAPPA: usize = 16;

#[derive(Debug)]
pub struct PeerBuckets<D: DHTIDHasher + Clone> {
    addr: SocketAddr,
    c_time: SystemTime,
    hasher: D,
    k_buckets: Vec<Vec<Peer<D>>>,
}

impl<D: DHTIDHasher + Clone> PeerBuckets<D> {
    pub fn new(hasher: D, addr: SocketAddr, now: SystemTime) -> Self {
        let k_buckets = vec![Vec::with_capacity(KAPPA)];
        PeerBuckets {
            hasher,
            c_time: now,
            addr,
            k_buckets,
        }
    }
    pub fn known_peer(&mut self, now: SystemTime, addr: SocketAddr) -> (Option<&Peer<D>>, bool) {
        self.reorganize(now);
        let target = self.hasher.clone().compute_id(addr, now);
        let k_idx = find_k_bucket_idx(self.id(now), target).min(self.k_buckets.len() - 1);
        let k_bucket = &self.k_buckets[k_idx];
        let opt = k_bucket.iter().find(|p| p.id(now) == target);
        (opt, opt.is_none() && self.k_buckets[k_idx].len() < KAPPA)
    }
    fn reorganize(&mut self, now: SystemTime) {
        let old_id = self.id(self.c_time);
        let cur_id = self.id(now);
        if old_id == cur_id {
            return;
        }
        self.c_time = now;
        let mut peers = Vec::with_capacity(self.len());
        peers.extend(self.drain());
        for peer in peers {
            self.insert_into(now, peer);
        }
        let to_trim = self
            .k_buckets
            .iter()
            .rev()
            .take_while(|v| v.is_empty())
            .count();
        self.k_buckets.truncate(self.k_buckets.len() - to_trim);
    }
    fn id(&self, now: SystemTime) -> [u8; 32] {
        self.hasher.compute_id(self.addr, now)
    }
    fn update_addr(&mut self) {
        unimplemented!();
    }
    fn drain<'a>(&'a mut self) -> impl Iterator<Item = Peer<D>> + 'a {
        self.k_buckets.iter_mut().flat_map(|b| b.drain(..))
    }
    /*pub fn find_nearest_peers(&mut self, target: &[u8; 32], alpha: usize) -> Vec<&Peer<D>> {
        let now = SystemTime::now();
        let mut k_idx = find_k_bucket_idx(&self.id, target).min(self.k_buckets.len() - 1);
        let mut ret = Vec::with_capacity(alpha);

        // The k_idx bucket will be the closest
        let k_bucket = &mut self.k_buckets[k_idx];
        k_bucket.sort_by_key(|p| p.id(now));
        ret.extend(k_bucket[..alpha.min(k_bucket.len())].iter());

        // The buckets after k_idx will be furthest
        for bucket in self.k_buckets.iter_mut() {
            bucket.sort_by_key(|k|)
        }

        if ret.len() >= alpha {
            return ret;
        }
        unimplemented!();
    }*/
    pub fn find_nearest_peers(
        &mut self,
        target: [u8; 32],
        alpha: usize,
        now: SystemTime,
    ) -> Vec<Peer<D>> {
        let mut ret = Vec::with_capacity(self.len());
        ret.extend(self.k_buckets.iter().flatten());
        ret.sort_unstable_by_key(|p| xor(p.id(now), target));
        ret.truncate(alpha);
        ret.into_iter().cloned().collect()
    }
    pub fn insert_into(&mut self, now: SystemTime, peer: Peer<D>) -> Option<Peer<D>> {
        self.reorganize(now);
        let self_id = self.id(now);
        let len = self.k_buckets.len();
        let peer_id = peer.id(now);
        let k_idx = find_k_bucket_idx(self_id, peer_id).min(len - 1);
        if self.k_buckets[k_idx]
            .iter()
            .find(|p| p.id(now) == peer_id)
            .is_some()
        {
            // do not add duplicative peer
            return Some(peer);
        }

        if self.k_buckets[k_idx].len() < KAPPA {
            self.k_buckets[k_idx].push(peer);
            None
        } else if k_idx == len - 1 {
            let top = &mut self.k_buckets[k_idx];
            let mut new = Vec::with_capacity(KAPPA);

            let mut i = 0;
            while i < top.len() {
                let k_idx = find_k_bucket_idx(self_id, top[i].id(now));
                if k_idx >= len {
                    new.push(top.swap_remove(i));
                } else {
                    i += 1
                }
            }
            self.k_buckets.push(new);
            self.insert_into(now, peer)
        } else {
            Some(peer)
        }
    }
    pub fn is_empty(&self) -> bool {
        match self.k_buckets.last() {
            Some(k_bucket) => k_bucket.is_empty(),
            None => true,
        }
    }
    pub fn len(&self) -> usize {
        self.k_buckets.iter().fold(0, |l, v| l + v.len())
    }
    pub fn iter(&self) -> impl Iterator<Item = &Peer<D>> {
        self.k_buckets.iter().rev().flatten()
    }
    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut Peer<D>> {
        self.k_buckets.iter_mut().rev().flatten()
    }
}

fn find_k_bucket_idx(k_bucket_id: [u8; 32], target: [u8; 32]) -> usize {
    let mut matched = 0;
    for b in xor_iter(k_bucket_id, target) {
        let leading = b.leading_zeros() as usize;
        matched += leading;
        if leading != 8 {
            break;
        }
    }
    matched
}

fn xor(left: [u8; 32], right: [u8; 32]) -> [u8; 32] {
    let mut ret = [0; 32];
    ret.iter_mut()
        .zip(xor_iter(left, right))
        .for_each(|(d, s)| *d = s);
    ret
}

fn xor_iter(left: [u8; 32], right: [u8; 32]) -> impl Iterator<Item = u8> {
    left.into_iter().zip(right).map(|(x, y)| x ^ y)
}

#[allow(dead_code)]
fn xor_64(left: &[u8; 32], right: &[u8; 32]) -> [u64; 4] {
    let mut ret = [0; 4];
    ret.iter_mut()
        .zip(xor_64_iter(left, right))
        .for_each(|(d, s)| *d = s);
    ret
}
fn xor_64_iter<'a>(left: &'a [u8; 32], right: &'a [u8; 32]) -> impl Iterator<Item = u64> + 'a {
    let slice_u64_fn = |s| {
        let mut u = [0; 8];
        u.copy_from_slice(s);
        u64::from_be_bytes(u)
    };
    let left = left.chunks_exact(8).map(slice_u64_fn);
    let right = right.chunks_exact(8).map(slice_u64_fn);
    left.zip(right).map(|(x, y)| x ^ y)
}

#[cfg(test)]
mod tests {
    use super::*;

    const B0: [u8; 32] = [
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF,
    ];
    const B1: [u8; 32] = [
        192, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00,
    ];
    const B2: [u8; 32] = [
        0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00,
    ];

    #[test]
    fn test_find_k_bucket_idx() {
        struct TestCase {
            k_bucket_id: [u8; 32],
            target: [u8; 32],
            expected: usize,
        }
        let test_cases = [
            TestCase {
                k_bucket_id: [0; 32],
                target: [0; 32],
                expected: 256,
            },
            TestCase {
                k_bucket_id: [0; 32],
                target: [0xFF; 32],
                expected: 0,
            },
            TestCase {
                k_bucket_id: B0,
                target: B1,
                expected: 2,
            },
            TestCase {
                k_bucket_id: B0,
                target: B2,
                expected: 8,
            },
        ];
        for (i, tc) in test_cases.into_iter().enumerate() {
            let result = find_k_bucket_idx(tc.k_bucket_id, tc.target);
            assert_eq!(tc.expected, result, "Test case {} failed!", i);
        }
    }

    use std::net::{IpAddr, Ipv4Addr, SocketAddrV4};

    #[derive(Clone)]
    struct FakeHasher {}
    impl DHTIDHasher for FakeHasher {
        fn compute_id(&self, addr: SocketAddr, _: SystemTime) -> [u8; 32] {
            let mut template = [0; 32];
            template[0] = match addr.ip() {
                IpAddr::V4(v4) => v4.octets()[0],
                IpAddr::V6(v6) => v6.octets()[0],
            };
            template
        }
    }
    use std::collections::HashSet;
    use std::str::FromStr;

    #[test]
    fn peer_buckets_insert_into_series() {
        let now = SystemTime::now();
        let our_addr = SocketAddr::from_str("255.255.0.1:65535").unwrap();
        let mut k_buckets = PeerBuckets::new(FakeHasher {}, our_addr, now);
        for i in 0..=255 {
            let addr = Ipv4Addr::new(i, 0, 0, 0);
            let peer = Peer::new(FakeHasher {}, SocketAddrV4::new(addr, 0).into());
            k_buckets.insert_into(now, peer);
        }
        assert_eq!(5, k_buckets.k_buckets.len());
        assert!(k_buckets.k_buckets.iter().all(|v| v.len() == 16));
        let now = SystemTime::now();
        for (bucket, expected) in k_buckets
            .k_buckets
            .into_iter()
            .zip([0, 0x80, 0xC0, 0xE0, 0xF0])
        {
            let expected: HashSet<u8> = (expected..=(expected + 15)).collect();
            let result = bucket.into_iter().map(|b| b.id(now)[0]).collect();
            assert_eq!(expected, result);
        }

        let mut k_buckets = PeerBuckets::new(FakeHasher {}, our_addr, now);
        for i in (0..=255).rev() {
            let addr = Ipv4Addr::new(i, 0, 0, 0);
            let peer = Peer::new(FakeHasher {}, SocketAddrV4::new(addr, 0).into());
            k_buckets.insert_into(now, peer);
        }

        for (bucket, expected) in k_buckets
            .k_buckets
            .into_iter()
            .zip([0x70, 0xB0, 0xD0, 0xE0, 0xF0])
        {
            let expected: HashSet<u8> = (expected..=(expected + 15)).collect();
            let result = bucket.into_iter().map(|b| b.id(now)[0]).collect();
            assert_eq!(expected, result);
        }
    }
    #[test]
    fn peer_buckets_insert_into_duplicative() {
        let now = SystemTime::now();
        let our_addr = SocketAddr::from_str("255.255.0.1:65535").unwrap();
        let mut k_buckets = PeerBuckets::new(FakeHasher {}, our_addr, now);
        let ip = Ipv4Addr::new(1, 0, 0, 0);
        let addr = SocketAddrV4::new(ip, 0).into();
        let peer = Peer::new(FakeHasher {}, addr);
        assert!(k_buckets.insert_into(now, peer.clone()).is_none());
        assert_eq!(addr, k_buckets.insert_into(now, peer.clone()).unwrap().addr);
    }
    /*
    #[test]
    fn peer_buckets_find_nearest_peers() {
        let seed: u64 = rand::random();
        println!("Seed: {}", seed);
        let mut rng = rand::rngs::SmallRng::seed_from_u64(64);

        let our_id = rng.gen();
        let addrs: Vec<SocketAddr> = (0..128).map(|_| {
            let ip = Ipv4Addr::from(rng.gen::<u32>());
            let port = rng.gen();
            SocketAddrV4::new(ip, port).into()
        }).collect();
        let mut k_buckets = PeerBuckets::new(our_id);

        let now = Instant::now();
        let hasher = hash::Sha256DHTHasher::new(0);
        let mut peers: Vec<_> = addrs.into_iter().filter_map(|addr| {
            let peer = Peer {
                key: 0,
                hasher: hasher.clone(),
                addr,
                first_seen: now,
                last_success: now
            };
            if k_buckets.insert_into(peer.clone()).is_none() {
                Some(peer)
            } else {
                None
            }
        }).collect();
        for (i, bucket) in k_buckets.k_buckets.iter().enumerate() {
            println!("{}: {}", i, bucket.len());
        }

        let now = SystemTime::now();
        for i in 0..16 {
            let target = rng.gen();

            peers.sort_by_cached_key(|p| xor(&p.id(now), &target));
            let expected = &peers[..3];
            let result = k_buckets.find_nearest_peers(&target, 4);

            for (j, (result, expected)) in result.into_iter().zip(expected).enumerate() {
                assert_eq!(expected.id(now), result.id(now), "Iterator {}, result {} failed!", i, j);
            }
        }

        peers.sort_by_cached_key(|p| xor(&p.id(now), &[0; 32]));
        let results = k_buckets.find_nearest_peers(&[0; 32], 128);
        for (j, (result, expected)) in results.into_iter().zip(peers).enumerate() {
            assert_eq!(expected.id(now), result.id(now), "Result {} did not match for all-zero test!", j);
        }
    }*/
}
