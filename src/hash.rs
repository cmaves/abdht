use super::serialize_socket_addr;
use sha2::{Digest, Sha256};
use std::cell::Cell;
use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
use std::time::SystemTime;

pub trait DHTIDHasher {
    fn compute_id(&self, addr: SocketAddr, now: SystemTime) -> [u8; 32];
}

#[derive(Clone, Debug)]
pub struct Sha256DHTHasher {
    nounce: u64,
    cache: Cell<ShaCache>,
}

impl Sha256DHTHasher {
    pub fn new(nounce: u64) -> Self {
        Sha256DHTHasher {
            nounce,
            cache: Cell::new(ShaCache::new()),
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct ShaCache {
    addr: SocketAddr,
    hash: [u8; 32],
}
impl ShaCache {
    fn new() -> Self {
        let zero_ip = Ipv4Addr::new(0, 0, 0, 0);
        ShaCache {
            addr: SocketAddrV4::new(zero_ip, 0).into(),
            hash: [0; 32],
        }
    }
    fn check_cache(&self, addr: SocketAddr) -> Option<[u8; 32]> {
        if self.addr == addr {
            Some(self.hash)
        } else {
            None
        }
    }
    fn store_cache(addr: SocketAddr, hash: [u8; 32]) -> Self {
        ShaCache { addr, hash }
    }
}

impl DHTIDHasher for Sha256DHTHasher {
    fn compute_id(&self, addr: SocketAddr, _: SystemTime) -> [u8; 32] {
        // The most frequent use case will be recomputing the same hash.
        // By caching this we can eliminate this constant recomputation.
        let cache = self.cache.get();
        if let Some(hash) = cache.check_cache(addr) {
            return hash;
        }

        let mut addr_buf = [0; 26];
        addr_buf[0..8].copy_from_slice(&self.nounce.to_le_bytes());
        let len = serialize_socket_addr(addr, &mut addr_buf[8..]).len() + 8;
        let hash = Sha256::digest(&addr_buf[..len]).into();
        self.cache.set(ShaCache::store_cache(addr, hash));
        hash
    }
}

#[cfg(test)]
mod tests {
    use std::net::{Ipv4Addr, Ipv6Addr, SocketAddrV4, SocketAddrV6};

    use super::*;
    #[test]
    fn test_sha256dhthasher() {
        let sockv4: SocketAddr = SocketAddrV4::new(Ipv4Addr::LOCALHOST, 10000).into();
        let sockv6: SocketAddr = SocketAddrV6::new(Ipv6Addr::LOCALHOST, 10000, 0, 0).into();

        let hasher = Sha256DHTHasher::new(0);
        let now = SystemTime::now();
        assert_eq!(
            hasher.compute_id(sockv4, now),
            [
                0xb9, 0x8f, 0x4e, 0x28, 0x70, 0xeb, 0x03, 0x39, 0xfd, 0x98, 0x39, 0xed, 0x40, 0x0f,
                0x85, 0x5f, 0xb7, 0x07, 0x38, 0x08, 0x91, 0x26, 0xc4, 0x63, 0xa9, 0x1f, 0xc3, 0xfd,
                0x4f, 0x2c, 0x98, 0xef
            ]
        );
        assert_eq!(
            hasher.compute_id(sockv6, now),
            [
                0xcf, 0x37, 0x3c, 0x6a, 0xe4, 0x8b, 0x43, 0xa7, 0xb9, 0xe8, 0x44, 0x31, 0x16, 0xa2,
                0x20, 0x04, 0xcd, 0x8e, 0x8b, 0x14, 0xe4, 0xb4, 0xd6, 0x26, 0x4d, 0x56, 0xea, 0xf6,
                0x6a, 0x1a, 0xa8, 0xe4
            ]
        );

        let hasher = Sha256DHTHasher::new(255);
        assert_eq!(
            hasher.compute_id(sockv4, now),
            [
                0x13, 0x65, 0x28, 0x2f, 0x39, 0x06, 0x86, 0x2f, 0x7a, 0x8f, 0xda, 0xef, 0x68, 0xd1,
                0xef, 0x4d, 0x2f, 0x88, 0x6e, 0x36, 0x65, 0x74, 0xa5, 0x43, 0x41, 0x3e, 0x02, 0x2d,
                0x02, 0x55, 0xc5, 0x34
            ]
        );
        assert_eq!(
            hasher.compute_id(sockv6, now),
            [
                0x5b, 0x28, 0x1a, 0x61, 0x85, 0x7d, 0x8d, 0x65, 0x29, 0x4c, 0x2b, 0x4f, 0x88, 0xac,
                0xee, 0xc1, 0x43, 0xea, 0x37, 0xa8, 0x3e, 0xd0, 0x26, 0xa3, 0x6e, 0xf6, 0x24, 0x48,
                0x81, 0x52, 0x41, 0xd8
            ]
        );

        let sockv4: SocketAddr = SocketAddrV4::new(Ipv4Addr::LOCALHOST, 255).into();
        let sockv6: SocketAddr = SocketAddrV6::new(Ipv6Addr::LOCALHOST, 255, 0, 0).into();
        assert_eq!(
            hasher.compute_id(sockv4, now),
            [
                0xcb, 0x86, 0xfc, 0xa0, 0xdc, 0x16, 0xf5, 0xfd, 0x32, 0xea, 0x6a, 0x1c, 0x90, 0x02,
                0x68, 0xb4, 0x31, 0x8f, 0xc3, 0x88, 0x8c, 0xda, 0x3e, 0x0b, 0x7a, 0x59, 0x1f, 0x6c,
                0x6e, 0x1e, 0xf9, 0x24
            ],
        );
        assert_eq!(
            hasher.compute_id(sockv6, now),
            [
                0x77, 0x9e, 0x15, 0xff, 0x00, 0xd7, 0x65, 0x78, 0x1c, 0x7e, 0x59, 0xe9, 0x06, 0xda,
                0x9e, 0xda, 0x0e, 0xd7, 0x72, 0xb7, 0x7d, 0x3b, 0xa1, 0x7b, 0x43, 0x57, 0xfc, 0x96,
                0x38, 0x23, 0xef, 0x5b
            ]
        );
    }
}
