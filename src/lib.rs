use std::net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6};
use std::rc::Rc;
use std::time::Duration;

use tokio::net::{lookup_host, ToSocketAddrs, UdpSocket};
use tokio::runtime::Builder as RTBuilder;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::oneshot::{channel as one_channel, Sender as OneSender};
use tokio::time::sleep;

pub mod hash;
mod peer;

#[allow(dead_code)]
mod whoami;

pub mod store;

mod worker;
use worker::ClientData;

use hash::DHTIDHasher;
pub use peer::*;

pub struct DHTClient<D>
where
    D: DHTIDHasher + Clone,
{
    sender: Sender<ClientCmd<D>>,
    _hasher: D,
}

pub const ALPHA: usize = 3;

impl<D> DHTClient<D>
where
    D: DHTIDHasher + Clone + Send + 'static,
{
    pub async fn bootstrap(
        listen: impl ToSocketAddrs,
        seeds: Vec<SocketAddr>,
        hasher: D,
    ) -> Result<Self, DHTError> {
        assert!(!seeds.is_empty());
        let socket = UdpSocket::bind(listen).await?;

        let (sender, receiver) = channel(32);
        let (boot_sender, boot_receiver) = one_channel();
        boot_receiver.await.unwrap()?;
        let h_clone = hasher.clone();
        std::thread::spawn(move || {
            let rt = RTBuilder::new_current_thread()
                .enable_all()
                .build()
                .unwrap();

            let mut client_data = ClientData::new(socket, h_clone);
            let res = rt.block_on(client_data.do_bootstrap(&seeds));
            let ok = res.is_ok();
            boot_sender.send(res).unwrap();
            if !ok {
                return;
            }
            rt.block_on(client_data.start(receiver)).ok();
        });
        let client = DHTClient {
            sender,
            _hasher: hasher,
        };
        if client.ping().await {
            Ok(client)
        } else {
            Err(DHTError::BootstrapFailure)
        }
    }
    pub async fn ping(&self) -> bool {
        let (sender, receiver) = one_channel();
        if let Err(_) = self.sender.send(ClientCmd::Ping(sender)).await {
            return false;
        }
        receiver.await.is_ok()
    }

    pub async fn shutdown(self) -> Result<(), DHTError> {
        let (one_sender, one_receiver) = one_channel();
        self.sender
            .send(ClientCmd::Shutdown(one_sender))
            .await
            .map_err(|_| DHTError::ClientHungup)?;
        one_receiver.await.map_err(|_| DHTError::ClientHungup)
    }

    pub async fn get_local_peer_list(&self) -> Result<Vec<Peer<D>>, DHTError> {
        let (one_sender, one_receiver) = one_channel();
        self.sender
            .send(ClientCmd::PeerListLocal(one_sender))
            .await
            .map_err(|_| DHTError::ClientHungup)?;
        one_receiver.await.map_err(|_| DHTError::ClientHungup)
    }

    pub async fn bootstrap_no_announce(
        listen: impl ToSocketAddrs,
        whoami: impl ToSocketAddrs,
        hasher: D,
    ) -> Result<Self, DHTError> {
        let whoami = lookup_host(whoami)
            .await?
            .next()
            .ok_or(DHTError::BootstrapFailure)?;
        let socket = UdpSocket::bind(listen).await?;

        let (sender, receiver) = channel(32);
        let h_clone = hasher.clone();
        std::thread::spawn(move || {
            let rt = RTBuilder::new_current_thread()
                .enable_all()
                .build()
                .unwrap();

            let mut client_data = ClientData::new(socket, h_clone);
            client_data.set_addr(whoami);
            rt.block_on(client_data.start(receiver)).ok();
        });
        let client = DHTClient {
            sender,
            _hasher: hasher,
        };
        if client.ping().await {
            Ok(client)
        } else {
            Err(DHTError::BootstrapFailure)
        }
    }
}

#[derive(PartialEq, Eq, Debug)]
enum DhtReqs {
    WhoAmI,
    WhoAmiRes([u8; 32], SocketAddr),
    FindNode([u8; 32], [u8; 32], u8),
    FindNodeRes([u8; 32], Vec<SocketAddr>),
}

#[derive(Debug)]
enum ClientCmd<D>
where
    D: DHTIDHasher + Clone,
{
    Ping(OneSender<()>),
    Shutdown(OneSender<()>),
    Kill,
    PeerListLocal(OneSender<Vec<Peer<D>>>),
}

impl DhtReqs {
    fn serialize<'a>(&self, buf: &'a mut [u8; 1024]) -> Result<&'a [u8], ()> {
        match self {
            DhtReqs::WhoAmI => {
                buf[0] = 0;
                Ok(&buf[..1])
            }
            DhtReqs::WhoAmiRes(id, addr) => {
                buf[0] = 1;
                buf[1..33].copy_from_slice(id);
                buf[33] = if addr.is_ipv4() { 0 } else { 1 };
                let len = serialize_socket_addr(*addr, &mut buf[34..]).len() + 34;
                Ok(&buf[..len])
            }
            DhtReqs::FindNode(target, minimum, cnt) => {
                if *cnt > 54 {
                    return Err(());
                }
                buf[0] = 2;
                buf[1..33].copy_from_slice(target);
                buf[33..65].copy_from_slice(minimum);
                buf[65] = 54.min(*cnt);
                Ok(&buf[..66])
            }
            DhtReqs::FindNodeRes(id, peers) => {
                buf[0] = 3;
                buf[1..33].copy_from_slice(id);
                if peers.len() > 54 {
                    return Err(());
                }
                buf[33] = peers.len() as u8;
                let mut used = 34;
                for peer_addr in peers {
                    used += serialize_socket_addr(*peer_addr, &mut buf[used..]).len();
                }
                if (used - 34) / 6 != peers.len() && (used - 34) / 18 != peers.len() {
                    Err(())
                } else {
                    Ok(&buf[..used])
                }
            }
            _ => unimplemented!(),
        }
    }
    fn deserialize(buf: &[u8]) -> Result<Self, ()> {
        if buf.is_empty() {
            return Err(());
        }
        match buf[0] {
            0 => Ok(DhtReqs::WhoAmI),
            1 if buf.len() >= 40 && buf[33] <= 1 => {
                let mut id = [0; 32];
                id.copy_from_slice(&buf[1..33]);
                let addr = if buf[33] == 0 {
                    deserialize_socket_addr_ipv4(&buf[34..])?.1.into()
                } else if buf.len() >= 52 {
                    deserialize_socket_addr_ipv6(&buf[34..])?.1.into()
                } else {
                    return Err(());
                };
                Ok(DhtReqs::WhoAmiRes(id, addr))
            }
            2 if buf.len() >= 66 => {
                let mut target = [0; 32];
                target.copy_from_slice(&buf[1..33]);
                let mut minimum = [0; 32];
                minimum.copy_from_slice(&buf[33..65]);
                Ok(DhtReqs::FindNode(target, minimum, buf[65]))
            }
            3 if buf.len() >= 34 => {
                let mut id = [0; 32];
                id.copy_from_slice(&buf[1..33]);
                let cnt = buf[33] as usize;
                if cnt > 54 {
                    return Err(());
                }
                let buf = &buf[34..];
                let mut addrs = Vec::with_capacity(cnt);
                if buf.len() == cnt * 6 {
                    for addr_buf in buf.chunks(6) {
                        addrs.push(deserialize_socket_addr_ipv4(addr_buf)?.1.into());
                    }
                } else if buf.len() == cnt * 18 {
                    for addr_buf in buf.chunks(18) {
                        addrs.push(deserialize_socket_addr_ipv6(addr_buf)?.1.into());
                    }
                } else {
                    return Err(());
                }
                Ok(DhtReqs::FindNodeRes(id, addrs))
            }
            _ => Err(()),
        }
    }
}

fn serialize_socket_addr<'a>(addr: SocketAddr, buf: &'a mut [u8]) -> &'a [u8] {
    match addr {
        SocketAddr::V4(v4) => {
            buf[0..4].copy_from_slice(&v4.ip().octets());
            buf[4..6].copy_from_slice(&v4.port().to_be_bytes());
            &buf[..6]
        }
        SocketAddr::V6(v6) => {
            buf[0..16].copy_from_slice(&v6.ip().octets());
            buf[16..18].copy_from_slice(&v6.port().to_be_bytes());
            &buf[..18]
        }
    }
}

fn deserialize_socket_addr_ipv4<'a>(buf: &'a [u8]) -> Result<(&'a [u8], SocketAddrV4), ()> {
    if buf.len() < 6 || buf[0..4] == [0; 4] || buf[4..6] == [0; 2] {
        return Err(());
    }
    let mut ip_buf = [0; 4];
    ip_buf.copy_from_slice(&buf[0..4]);
    let mut port_buf = [0; 2];
    port_buf.copy_from_slice(&buf[4..6]);
    let addr = SocketAddrV4::new(Ipv4Addr::from(ip_buf), u16::from_be_bytes(port_buf));
    Ok((&buf[6..], addr))
}

fn deserialize_socket_addr_ipv6<'a>(buf: &'a [u8]) -> Result<(&'a [u8], SocketAddrV6), ()> {
    if buf.len() < 18 || buf[0..16] == [0; 16] || buf[16..18] == [0; 2] {
        return Err(());
    }
    let mut ip_buf = [0; 16];
    ip_buf.copy_from_slice(&buf[0..16]);
    let mut port_buf = [0; 2];
    port_buf.copy_from_slice(&buf[16..18]);
    let addr = SocketAddrV6::new(Ipv6Addr::from(ip_buf), u16::from_be_bytes(port_buf), 0, 0);
    Ok((&buf[18..], addr))
}

#[derive(Debug)]
pub enum DHTError {
    IoError(std::io::Error),
    ClientHungup,
    BootstrapFailure,
}

impl From<std::io::Error> for DHTError {
    fn from(err: std::io::Error) -> Self {
        DHTError::IoError(err)
    }
}

impl std::error::Error for DHTError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::IoError(err) => Some(err),
            _ => None,
        }
    }
}
impl std::fmt::Display for DHTError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self, f)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;

    #[test]
    fn test_dht_req_serialize() {
        let ipv4_local = SocketAddr::from_str("127.0.0.1:10000").unwrap();
        let ipv6_local = SocketAddr::from_str("[::1]:10000").unwrap();
        let google_v4_dns = SocketAddr::from_str("8.8.8.8:25").unwrap();
        let google_v6_dns = SocketAddr::from_str("[2001:4860:4860::8888]:25").unwrap();
        let google_v4_ntp = SocketAddr::from_str("216.239.35.0:123").unwrap();
        let madeup_v6_ntp = SocketAddr::from_str("[2567:890a:bcde::f123]:123").unwrap();
        let test_cases = [
            DhtReqs::WhoAmI,
            DhtReqs::WhoAmiRes([0; 32], ipv4_local),
            DhtReqs::WhoAmiRes([0xFF; 32], ipv6_local),
            DhtReqs::FindNode([0; 32], [0xFF; 32], 54),
            DhtReqs::FindNode([0xFF; 32], [0; 32], 8),
            DhtReqs::FindNodeRes([0; 32], vec![ipv4_local, google_v4_ntp, google_v4_dns]),
            DhtReqs::FindNodeRes([0xFF; 32], vec![ipv6_local, madeup_v6_ntp, google_v6_dns]),
        ];
        let mut msg_buf = [0; 1024];
        for (i, test_case) in test_cases.into_iter().enumerate() {
            let msg = test_case.serialize(&mut msg_buf).unwrap();
            let deserial = DhtReqs::deserialize(msg);
            assert_eq!(
                Ok(test_case),
                deserial,
                "Test case {} failed: bytes: {:X?}",
                i,
                msg
            );
        }
    }
    #[test]
    fn test_dht_req_serialize_fail() {
        let ipv4_local = SocketAddr::from_str("127.0.0.1:10000").unwrap();
        let ipv6_local = SocketAddr::from_str("[::1]:10000").unwrap();

        let test_cases = [
            DhtReqs::FindNode([0; 32], [0xFF; 32], 255),
            DhtReqs::FindNodeRes([0; 32], vec![ipv4_local, ipv6_local]),
        ];
        let mut msg_buf = [0; 1024];
        for (i, test_case) in test_cases.into_iter().enumerate() {
            let res = test_case.serialize(&mut msg_buf);
            assert_eq!(Err(()), res, "Test case {} failed!", i);
        }
    }

    #[test]
    fn test_ncargo() {
        println!("{}", cfg!(target_feature = "test"));
    }
}
