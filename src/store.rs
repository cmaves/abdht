use std::collections::HashMap;
use std::time::{Duration, Instant};

pub struct LFUStore<K: Clone, V> {
    min_heap: Vec<(Counter, K)>,
    map: HashMap<K, (usize, V)>,
    max_size: usize,
    period: Duration,
}

impl<K: Clone, V> LFUStore<K, V>
where
    K: Eq + std::hash::Hash,
{
    pub fn new(max_size: usize, period: Duration) -> Self {
        assert_ne!(0, max_size, "max_size cannot be 0!");
        assert!(!period.is_zero());
        Self {
            min_heap: Vec::new(),
            map: HashMap::new(),
            max_size,
            period,
        }
    }
    pub fn view<Q>(&self, key: &Q) -> Option<&V>
    where
        K: std::borrow::Borrow<Q>,
        Q: Eq + std::hash::Hash,
    {
        self.map.get(key).map(|(_, v)| v)
    }
    pub fn get<Q>(&mut self, key: &Q) -> Option<&mut V>
    where
        K: std::borrow::Borrow<Q>,
        Q: Eq + std::hash::Hash,
    {
        let h_idx = self.map.get(key)?.0;
        self.increment_h_idx(h_idx);
        Some(self.map.get_mut(key).map(|(_, v)| v).unwrap())
    }
    fn increment_h_idx(&mut self, cur_idx: usize) {
        let now = Instant::now();
        let cur_cnt = self.min_heap[cur_idx].0.increment(now);
        self.down(cur_idx, cur_cnt, now);
    }
    fn down(&mut self, mut cur_idx: usize, cur_cnt: u64, now: Instant) -> bool {
        let mut l_idx = l_child(cur_idx);
        let mut swapped = false;
        while l_idx < self.min_heap.len() {
            // check if we should swap with left child
            let l_cnt = self.min_heap[l_idx].0.cnt(now);
            let (mut s_idx, s_cnt) = if l_cnt < cur_cnt {
                (l_idx, l_cnt)
            } else {
                (cur_idx, cur_cnt)
            };

            // check if we should swap with right child
            let r_idx = r_child(cur_idx);
            if r_idx < self.min_heap.len() {
                let r_cnt = self.min_heap[r_idx].0.cnt(now);
                if r_cnt < s_cnt {
                    s_idx = r_idx;
                }
            }
            if s_idx == cur_idx {
                break;
            }
            swapped = true;
            self.swap(s_idx, cur_idx);
            cur_idx = s_idx;
            l_idx = l_child(cur_idx);
        }
        swapped
    }
    fn up(&mut self, mut cur_idx: usize, cur_cnt: u64, now: Instant) -> bool {
        let mut swapped = false;
        while cur_idx > 0 {
            let p_idx = parent(cur_idx);
            let p_cnt = self.min_heap[p_idx].0.cnt(now);
            if p_cnt <= cur_cnt {
                break;
            }
            swapped = true;
            self.swap(p_idx, cur_idx);
            cur_idx = p_idx;
        }
        swapped
    }
    fn swap(&mut self, a: usize, b: usize) {
        self.min_heap.swap(a, b);
        self.map.get_mut(&self.min_heap[a].1).unwrap().0 = a;
        self.map.get_mut(&self.min_heap[b].1).unwrap().0 = b;
    }
    pub fn insert(&mut self, key: K, value: V) -> Result<Option<(K, V)>, (K, V)> {
        let now = Instant::now();
        if let Some(existing) = self.map.get_mut(&key) {
            let ret = std::mem::replace(&mut existing.1, value);
            let idx = existing.0;
            self.min_heap[idx].0 = Counter::new(1, now, self.period);
            if !self.up(idx, 1, now) {
                self.down(idx, 1, now);
            }
            return Ok(Some((key, ret)));
        }
        let ret = if self.min_heap.len() >= self.max_size {
            if self.min_heap[0].0.cnt(now) > 0 {
                return Err((key, value));
            }
            Some(self.pop_min(now))
        } else {
            None
        };
        let cntr = Counter::new(1, now, self.period);
        let idx = self.min_heap.len();
        self.min_heap.push((cntr, key.clone()));
        self.map.insert(key, (idx, value));
        self.up(idx, 1, now);
        Ok(ret)
    }
    fn pop_min(&mut self, now: Instant) -> (K, V) {
        let (_, key) = self.min_heap.swap_remove(0);
        let val = self.map.remove(&key).unwrap();
        if let Some(first) = self.min_heap.first() {
            self.map.get_mut(&first.1).unwrap().0 = 0;
            let cur_cnt = first.0.cnt(now);
            self.down(0, cur_cnt, now);
        }
        (key, val.1)
    }

    pub fn len(&self) -> usize {
        debug_assert_eq!(self.min_heap.len(), self.map.len());
        self.min_heap.len()
    }
}

fn l_child(idx: usize) -> usize {
    idx * 2 + 1
}
fn r_child(idx: usize) -> usize {
    idx * 2 + 2
}
fn parent(idx: usize) -> usize {
    (idx - 1) / 2
}

#[derive(Debug)]
struct Counter {
    last_time: Instant,
    period: Duration,
    cnt: u64,
}

impl Counter {
    fn new(init_cnt: u64, now: Instant, period: Duration) -> Self {
        Self {
            last_time: now,
            cnt: init_cnt,
            period,
        }
    }
    fn increment(&mut self, now: Instant) -> u64 {
        self.cnt += 1;
        self.cnt = self.cnt(now);
        self.cnt
    }
    fn cnt(&self, now: Instant) -> u64 {
        let ticks = now.duration_since(self.last_time).as_nanos() / self.period.as_nanos();
        self.cnt.saturating_sub(ticks as u64)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::prelude::*;

    const L_PERIOD: Duration = Duration::from_secs(60);
    #[test]
    fn lfu_store_no_overflow() {
        let mut lfu = LFUStore::new(8, L_PERIOD);
        for i in 0..8 {
            assert_eq!(Ok(None), lfu.insert(i, 0));
        }
        assert_eq!(8, lfu.len());

        assert_eq!(Err((8, 0)), lfu.insert(8, 0));
        for i in 0..8 {
            assert_eq!(Ok(Some((i, 0))), lfu.insert(i, 0));
        }
    }

    #[test]
    fn lfu_store_check_assess_pop() {
        let seed: u64 = rand::random();
        println!("Seed: {}", seed);
        let mut rng = rand::rngs::SmallRng::seed_from_u64(64);

        let mut lfu = LFUStore::new(32, L_PERIOD);
        let mut count = Vec::with_capacity(32);
        for i in 0..32 {
            count.push(i);
            assert_eq!(Ok(None), lfu.insert(i, 0));
        }
        assert_eq!(32, lfu.len());
        count.shuffle(&mut rng);

        for cnt in count {
            for _ in 0..cnt {
                assert_eq!(Some(&mut 0), lfu.get(&cnt));
            }
        }

        let now = Instant::now();
        for i in 0..32 {
            println!("{}: {:#?}", i, lfu.min_heap);
            assert_eq!((i, 0), lfu.pop_min(now));
        }
    }
    #[test]
    fn lfu_store_expires_push() {
        let period = Duration::from_millis(500);
        let mut lfu = LFUStore::new(1, period);
        assert_eq!(Ok(None), lfu.insert(0, 0));
        assert_eq!(Err((1, 0)), lfu.insert(1, 0));
        std::thread::sleep(period);
        assert_eq!(Ok(Some((0, 0))), lfu.insert(2, 0));
    }
}
