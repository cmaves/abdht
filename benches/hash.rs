#![feature(test)]
extern crate test;

use std::net::SocketAddr;
use std::str::FromStr;
use std::time::SystemTime;

use test::Bencher;

use abdht::hash::{DHTIDHasher, Sha256DHTHasher};

#[bench]
fn sha_256_dht_hasher_same(b: &mut Bencher) {
    let addr = SocketAddr::from_str("127.0.0.1:10000").unwrap();
    let now = SystemTime::now();

    let hasher = Sha256DHTHasher::new(0);
    b.iter(|| hasher.compute_id(addr, now));
}

#[bench]
fn sha_256_dht_hasher_different(b: &mut Bencher) {
    let addrs = [
        SocketAddr::from_str("127.0.0.1:10000").unwrap(),
        SocketAddr::from_str("127.0.0.2:10000").unwrap(),
        SocketAddr::from_str("127.0.0.3:10000").unwrap(),
    ];
    let now = SystemTime::now();

    let hasher = Sha256DHTHasher::new(0);
    let mut i = 0;
    b.iter(|| {
        i += 1;
        hasher.compute_id(addrs[i % 3], now)
    });
}
